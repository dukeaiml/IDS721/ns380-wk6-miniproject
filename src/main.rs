use tracing_subscriber::filter::{EnvFilter, LevelFilter};
use tracing::{info, debug, error};
use lambda_http::{run, service_fn, Body, Error, Request, Response};
use serde::{Deserialize, Serialize};
use serde_json::from_str;
use chrono::{Local, NaiveDate};
use time::OffsetDateTime;
use yahoo_finance_api::YahooConnector;

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct RequestBody {
    pub start: String,
    pub end: String,
    pub ticker: String,
}

/// This is the main body for the function.
/// Write your code inside it.
/// There are some code example in the following URLs:
/// - https://github.com/awslabs/aws-lambda-rust-runtime/tree/main/examples
async fn function_handler(event: Request) -> Result<Response<Body>, Error> {
    // Extract some useful information from the request
    let body = event.body();
    let s = std::str::from_utf8(body).expect("invalid utf-8 sequence");

    //Serialze JSON into struct.
    //If JSON is incorrect, send back 400 with error.
    let item = match from_str::<RequestBody>(s) {
        Ok(item) => item,
        Err(err) => {
            error!("Got badly formatted JSON input");
            let resp = Response::builder()
                .status(400)
                .header("content-type", "text/html")
                .body(("Please make sure your body is formatted as {'start': 'YYYY-MM-DD','end': 'YYYY-MM-DD','ticker': '<TICKER>'} ".to_string() + &err.to_string() + "\n").into())
                .map_err(Box::new)?;
            return Ok(resp);
        }
    };

    // Validate that the start date is actually a real date
    let start = match NaiveDate::parse_from_str(item.start.as_str(), "%Y-%m-%d") {
        Ok(date) => date,
        Err(err) => {
            error!(input=item.start.as_str(), "Got badly formatted start date input");
            let resp = Response::builder()
                .status(400)
                .header("content-type", "text/html")
                .body(("Please make sure your body is formatted as {'start': 'YYYY-MM-DD','end': 'YYYY-MM-DD','ticker': '<TICKER>'} ".to_string() + &err.to_string() + "\n").into())
                .map_err(Box::new)?;
            return Ok(resp);
        }
    };
    debug!("Successfully parsed start date");
    
    // Validate that the end date is actually a real date
    let end = match NaiveDate::parse_from_str(item.end.as_str(), "%Y-%m-%d") {
        Ok(date) => date,
        Err(err) => {
            error!(input=item.end.as_str(), "Got badly formatted end date input");
            let resp = Response::builder()
                .status(400)
                .header("content-type", "text/html")
                .body(("Please make sure your body is formatted as {'start': 'YYYY-MM-DD','end': 'YYYY-MM-DD','ticker': '<TICKER>'} ".to_string() + &err.to_string() + "\n").into())
                .map_err(Box::new)?;
            return Ok(resp);
        }
    };
    debug!("Successfully parsed start date");

    // Validate that the end date is today or in the past
    let today = Local::now().date_naive();
    if today < end {
        error!(input=item.end.as_str(), "Got too far in the future end date input");
        let resp = Response::builder()
            .status(400)
            .header("content-type", "text/html")
            .body("Please make sure your end date is not in the future\n".to_string().into())
            .map_err(Box::new)?;
        return Ok(resp);
    }

    // Validate that the end date is in the future compared to the start date
    if end < start {
        error!(start=item.start.as_str(), end=item.end.as_str(), "Got end date before start date");
        let resp = Response::builder()
            .status(400)
            .header("content-type", "text/html")
            .body("Please make sure your start date is before your end date\n".to_string().into())
            .map_err(Box::new)?;
        return Ok(resp);
    }
    debug!("Got valid start and end dates");

    let provider = YahooConnector::new();
    // Beginning of start date
    let start_dt = OffsetDateTime::from_unix_timestamp(start.and_hms_opt(0, 0, 0).unwrap().timestamp()).unwrap();
    // End of end date
    let end_dt = OffsetDateTime::from_unix_timestamp(end.and_hms_opt(23, 59, 59).unwrap().timestamp()).unwrap();
    info!(ticker = item.ticker.as_str(), start = start.format("%Y-%m-%d").to_string(), end = end.format("%Y-%m-%d").to_string(), "Getting stock price information");
    let resp = provider.get_quote_history(item.ticker.as_str(), start_dt, end_dt).await?;
    let mut out: String = "".to_owned();
    for quote in resp.quotes().unwrap().iter() {
        out.push_str(format!("Timestamp: {}, Adj close: {}", quote.timestamp, quote.adjclose).as_str());
        out.push_str("\n");
    }

    debug!("Returning quotes");
    // Return something that implements IntoResponse.
    // It will be serialized to the right response event automatically by the runtime
    let resp = Response::builder()
        .status(200)
        .header("content-type", "text/plain")
        .body(out.to_string().into())
        .map_err(Box::new)?;
    Ok(resp)
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing_subscriber::fmt()
        .with_env_filter(
            EnvFilter::builder()
                .with_default_directive(LevelFilter::INFO.into())
                .from_env_lossy(),
        )
        // disable printing the name of the module in every log line.
        .with_target(false)
        // disabling time is handy because CloudWatch will add the ingestion time.
        .without_time()
        // add json formatting
        .json()
        .init();

    run(service_fn(function_handler)).await
}
