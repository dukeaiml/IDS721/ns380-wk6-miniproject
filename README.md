# ns380-wk6-miniproject
Stock price extractor - given a date range and a stock ticker, get some prices for the given stock.

Usage Instructions:
Run a command like
```
curl -H "Content-Type: application/json" --data '{"start": "2022-04-01", "end": "2022-04-06", "ticker": "AAPL"}' https://n4358v0o9g.execute-api.us-west-2.amazonaws.com/default/ns380-finance-lambda
```
This should display a list of stock prices by timestamp over the time range requested.

A test JSON is included. To use this, simply change the command above to
```
curl -H "Content-Type: application/json" --data @test.json https://n4358v0o9g.execute-api.us-west-2.amazonaws.com/default/ns380-finance-lambda
```
after downloading test.json to your local current working directory.

This function has been instrumented with tracing and logging. Example logs in cloudwatch look like
![alt text](logs.png)

and an example trace in X-Ray looks like
![alt text](trace.png)
